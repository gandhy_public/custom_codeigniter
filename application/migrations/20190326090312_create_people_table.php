<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_people_table extends CI_Migration
{
    public function up()
    {
    	$this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'address' => array(
            	'type' => 'VARCHAR',
            	'constraint' => '255'
            )
        ));
        $this->dbforge->add_key('id');
        $this->dbforge->create_table('people');

        foreach ($this->seedData as $seed ) {
            $sql = "INSERT INTO `people` (`name`, `address`) VALUES ".$seed;
            $this->db->query($sql);
        }
    }

    public function down()
    {
    	 $this->dbforge->drop_table('people');
    }

    private $seedData = array(
        "('lorem1', 'ipsum1')",
        "('lorem2', 'ipsum2')",
        "('lorem3', 'ipsum3')",
        "('lorem4', 'ipsum4')",
        "('lorem5', 'ipsum5')",
        "('lorem6', 'ipsum6')",
        "('lorem7', 'ipsum7')",
        "('lorem8', 'ipsum8')",
        "('lorem9', 'ipsum9')",
        "('lorem10', 'ipsum10')",
        "('lorem11', 'ipsum11')",
        "('lorem12', 'ipsum12')"
    );
}