<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Domain\Example\Custom\CustomClass;

class ExampleController extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('Example_model');
    }
    
    public function index()
    {
        $data = [
            [
                'name' => 'Lorem1',
                'address' => 'Ipsum1'
            ],
            [
                'name' => 'Lorem2',
                'address' => 'Ipsum2'
            ],
            [
                'name' => 'Lorem3',
                'address' => 'Ipsum3'
            ]
        ];
        return apiResponseBuilder(200,$data);
    }

    public function paging()
    {
        $limit = (int) $this->input->get('limit') ?: '5';
        $offset = (int) $this->input->get('page') ?: '1';

        $data = $this->Example_model->allData();
        return apiResponseBuilder(200,paginate($data,$limit,$offset));
    }

    public function custom()
    {
        return apiResponseBuilder(200,CustomClass::foo());
    }

}