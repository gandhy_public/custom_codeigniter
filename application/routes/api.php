<?php

/**
 * API Routes
 *
 * This routes only will be available under AJAX requests. This is ideal to build APIs.
 */

Route::get('response', 'ExampleController@index' );
Route::get('paging', 'ExampleController@paging' );
Route::get('custom', 'ExampleController@custom' );
