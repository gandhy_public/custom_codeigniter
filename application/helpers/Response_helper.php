<?php

use Illuminate\Pagination\LengthAwarePaginator;

function apiResponseBuilder($status = 500, $data = [])
{
	$CI = &get_instance();
	$response = [
		'status' => $status,
		'data' => $data
	];
	return $CI->output
        ->set_content_type('application/json')
        ->set_output(json_encode($response));
}

function paginate($data,$limit,$page)
{
	$offset = ($page == '1') ? '0' : ($page * $limit) - $limit;

	$records = clone $data;

	$itemCollection = collect($data->limit($limit,$offset)->get()->result_array());
    $currentPageItems = $itemCollection->slice(0, $limit)->all();
    $paginatedItems= new LengthAwarePaginator($currentPageItems , $records->count_all_results(), $limit, $page);
    $paginatedItems->setPath(current_url());

    return $paginatedItems;
}