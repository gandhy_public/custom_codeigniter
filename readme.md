# Custom Framework Codeigniter

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://masoft.co.id)

# Stack

  - [Codeigniter](https://codeigniter.com)
  - [MySQL](https://www.mysql.com)

# Library

  - [Luthier CI](https://luthier.ingenia.me/ci/en/)
  - [Illuminate Pagination](https://packagist.org/packages/illuminate/pagination)
  - [Codeigniter CLI](https://github.com/kenjis/codeigniter-cli)

# Features

  - Migration
  - Routing
  - Pagination
  - Response API builder
  - Domain Custom Class

# Installation

- Clone this repository
- Setting Database
- Run this command

```sh
$ cd path/to/project
$ composer install
$ php index.php luthier migrate
```

# Example

example API and Route Folder

| Description | API | File
| ------ | ------ | ------ |
| example API get reqeust | {your project}/index.php/response | application/controllers/ExampleController@index |
| example API for pagination | {your project}/index.php/paging | application/controllers/ExampleController@paging |
| example API with Domain Custom Class | {your project}/index.php/custom | application/controllers/ExampleController@custom |

# Contributors

- [Gandhy Bagoes Cahyono](https://gitlab.com/gandhybagoes)